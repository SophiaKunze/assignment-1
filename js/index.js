import Work from "./work.js"
import Bank from "./bank.js"

/**----------------------------------------------------
   * FETCH COMPUTERS
   ----------------------------------------------------*/

async function fetchLaptops() { 
  try {
    const response = await fetch(
      urlLaptopEndpoint
    );
    const json = await response.json();
    // create list of laptop objects
    //const laptops = json.map(x => Laptop(x))
    return json;
  } catch (error) {
    console.error(`Error: ${error.message}`);
  }
}

/**----------------------------------------------------
   * DOM ELEMENTS
   ----------------------------------------------------*/
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("outstanding-loan");
const getLoanButton = document.getElementById("get-loan");
const payElement = document.getElementById("pay");
const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");
const repayLoanButton = document.getElementById("repay-loan");
repayLoanButton.style.display = "none";
const laptopFeaturesHeader = document.getElementById("features");
laptopFeaturesHeader.style.display = "none";
const laptopFeaturesElement = document.getElementById("laptop-features");
const laptopSelectElement = document.getElementById("laptop");
const laptopImageElement = document.getElementById("laptop-image");
const laptopTitleElement = document.getElementById("laptop-title");
const laptopDescriptionElement = document.getElementById("laptop-description");
const priceElement = document.getElementById("price");
const buyNowButton = document.getElementById("buy-now");

/**----------------------------------------------------
   * VARIABLES
   ----------------------------------------------------*/
const formatter = formatToCurrency();
const urlLaptopBase = "https://noroff-komputer-store-api.herokuapp.com/"
const urlLaptopEndpoint = `${urlLaptopBase}computers`
const bank = new Bank(balanceElement, loanElement, formatter, repayLoanButton);
const work = new Work(payElement, bank, repayLoanButton, formatter);
let displayedLaptop = "";
// fetch laptops
const laptops = await fetchLaptops();

/**----------------------------------------------------
 * FUNCTIONS
 ----------------------------------------------------*/
// Define what happens when selection of laptop is changed
function onSelectChange() {
  const laptopId = laptopSelectElement.value;
  // laptop is selected from laptops based on its id
  displayedLaptop = laptops.find((targetLaptop) => parseInt(targetLaptop.id) === parseInt(laptopId)); 
  renderLaptop();
}

// Define what happens when image could not be fetched
function onImageNotFound() {
  laptopImageElement.src = "images/image_not_found.png";
}

function onGetLoan() {
  bank.receiveLoan();
}

function onWork() {
  work.receivePayment();
}

function onRepayLoan() {
  work.repayLoan();
}

function onBank() {
  work.bankTransfer();
}

function onBuyNow() {
  if (displayedLaptop.price === undefined) {
    alert("Please select a laptop first.")
  }
 // check if balance is sufficient to buy a laptop
  else if (bank.getBalance() >= displayedLaptop.price) {
    alert("You have purchased the laptop.")
    bank.setBalance(bank.getBalance() - displayedLaptop.price)
 }
  else {
  alert("You cannot afford the laptop.")
 }
}

function updateLaptopOptions() {
  // add laptops as options to be selected
  for (const laptop of laptops) {
    laptopSelectElement.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`;
  }
}

function formatToCurrency() {
  return Intl.NumberFormat("en-GB", {
    style: "currency",
    currency: "EUR",
  });
}

/**----------------------------------------------------
 * ARROW FUNCTIONS
 ----------------------------------------------------*/
const renderLaptop = () => {
  // if image cannot be fetched, add not found image
  laptopImageElement.addEventListener("error", onImageNotFound);
  laptopImageElement.src = urlLaptopBase + displayedLaptop.image; 
  laptopTitleElement.innerText = displayedLaptop.title;
  laptopDescriptionElement.innerText = displayedLaptop.description;
  priceElement.innerText = `${formatter.format(displayedLaptop.price)}`; 
  // update display of features
  laptopFeaturesHeader.style.display = "inline-block";
  laptopFeaturesElement.innerHTML = "";
  for (const feature of displayedLaptop.specs) {
    laptopFeaturesElement.innerHTML += `
    <li>${feature}</li>`
  }
};

/**----------------------------------------------------
 * MAIN PROGRAM
 ----------------------------------------------------*/
// initial update of elements
bank.updateBalanceElement();
bank.updateLoanElement();
work.updatePayElement();
updateLaptopOptions();

laptopSelectElement.addEventListener("change", onSelectChange);
getLoanButton.addEventListener("click", onGetLoan);
workButton.addEventListener("click", onWork);
repayLoanButton.addEventListener("click", onRepayLoan);
bankButton.addEventListener("click", onBank);
buyNowButton.addEventListener("click", onBuyNow)
