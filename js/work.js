class Work {
    #pay;
    payElement;
    #repayLoanButton;
    #bank;
    amount;
    formatter;
  
    constructor(payElement, bank, repayLoanButton, formatter) {
      this.#pay = 0;
      this.payElement = payElement;
      this.#repayLoanButton = repayLoanButton;
      this.#bank = bank;
      this.amount = 100;
      this.formatter = formatter;
    }
  
    hideRepayLoanButton() {
      this.#repayLoanButton.style.display = "none"
    }

    // getter method
    getPay() {
      return this.#pay;
    }
  
    // setter method
    setPay(newPay) {
      // update attribute
      this.#pay = newPay;
      // update element
      this.updatePayElement();
    }

    updatePayElement() {
      this.payElement.innerText = `Pay: ${this.formatter.format(this.getPay())}`;
    }
  
    receivePayment() {
      this.setPay(this.#pay + this.amount);
    }
  
    repayLoan() { 
      // if loan is bigger than pay, full amount is used to repay loan
      if (this.#bank.getLoan() > this.getPay()) {
        this.#bank.setLoan(this.#bank.getLoan() - this.getPay());
        this.setPay(0);
      } 
      // else loan is payed back, remaining amount is transferred to bank account
      else {
        this.setPay(this.getPay() - this.#bank.getLoan());
        this.#bank.setLoan(0);
        // hide repay loan button
        this.hideRepayLoanButton()
        this.#bank.setBalance(this.#bank.getBalance() + this.getPay())
        this.setPay(0)
      }
    }
  
    bankTransfer() {
        // 10 % of money should be used for repayment
        const repayment = this.getPay() * 0.1;
        // 90 % of money should be transferred to bank account
        let deposit = this.getPay() - repayment;
        // reset pay
        this.setPay(0)
        if (this.#bank.getLoan() > repayment) {
            console.log("this.#bank.getLoan() > repayment")
            // subtract repayment from loan
            this.#bank.setLoan(this.#bank.getLoan() - repayment); 
        }
        else if (this.#bank.getLoan() <= repayment) {
            // set loan to zero and add difference 
            console.log("this.#bank.getLoan() <= repayment")
            deposit += (repayment - this.#bank.getLoan());
            this.#bank.setLoan(0);
            this.hideRepayLoanButton();
        }
        else {
            console.log("Both conditions did not apply")
        }
        // add deposit to balance
        this.#bank.setBalance(this.#bank.getBalance() + deposit);
    }
  }

export default Work