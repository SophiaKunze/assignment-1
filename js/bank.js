class Bank {
    #balance;
    #loan;
    loanElement;
    balanceElement;
    formatter;
    repayLoanButton;
  
    constructor(balanceElement, loanElement, formatter, repayLoanButton) {
      this.#balance = 0; 
      this.#loan = 0;
      this.loanElement = loanElement;
      this.balanceElement = balanceElement;
      this.formatter = formatter;
      this.repayLoanButton = repayLoanButton;
    }
  
    // getter methods
    getBalance() {
      return this.#balance;
    }
  
    getLoan() {
      return this.#loan;
    }
  
    // setter methods
    setBalance(newBalance) {
      // update attribute
      this.#balance = newBalance;
      // update element
      this.updateBalanceElement();
    }
  
    setLoan(newLoan) {
      // update attribute
      this.#loan = newLoan;
      // update element
      this.updateLoanElement();
    }

    updateBalanceElement() {
      this.balanceElement.innerText = `Balance: ${this.formatter.format(this.getBalance())}`;
    }

    updateLoanElement() {
      this.loanElement.innerText = `Outstanding loan: ${this.formatter.format(
        this.getLoan()
      )}`;
      if (this.getLoan() === 0) {
      // hide loan element
        this.loanElement.style.display = "none"
      }
      // show loan element
      else {
        this.loanElement.style.display = "inline-block"
      }
    }

    receiveLoan() {
      if (this.getLoan() > 0) {
        alert("Please pay back your loan before getting a new one!");
      } else {
        let loanInput = prompt("Please enter the amount you wish to loan.");
        // check if input is not a number
        if (isNaN(loanInput)) {
          alert("Please enter a number!")
        }
        else {
          // round number to two digits
          loanInput = Math.round(loanInput * 100) / 100;
          // make sure input is greater or equal to zero
          if (loanInput <= 0) {
            alert("You cannot get a loan equal to or lower than zero.")
          }
          // check if requested loan is too high
          else if (loanInput > 2 * this.getBalance()) {
            alert("You cannot get a loan more than double of your bank balance.");
          } else {
            this.setLoan(loanInput);
            this.setBalance(this.getBalance() + this.getLoan())
            // make repayLoanButton visible
            this.repayLoanButton.style.display = "inline-block"; 
          }
        }
      }
    }
  }

  export default Bank