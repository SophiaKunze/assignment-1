# Komputer Store - A Dynamic Webpage using JavaScript
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/Readme-Standard-green?logo=markdown)](https://github.com/RichardLitt/standard-readme) [![JavaScript](https://img.shields.io/badge/-JavaScript-black?logo=javascript)](https://developer.mozilla.org/en-US/docs/web/javascript) 
[![](https://img.shields.io/badge/-Bootsrap-black?logo=bootstrap)](https://getbootstrap.com/)


This is a dynamic webpage using "vanilla" JavaScript. 

This repository contains:

1. The [HTML entry point](index.html).
2. A [CSS style sheet](styles.css).
3. A folder with [JavaScript files](js/).
4. A folder with a [default image](images/) if an image to be displayed cannot be fetched.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background
This webpage simulates a store for purchasing laptops and is my first JS project in the scope of a fullstack development course by <a href="https://www.noroff.no/en/">Noroff</a>. A user can work and get a loan to then use the money to purchase a laptop.

## Install
Clone repository via `git clone`. You can use <a href="https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer">LiveServer</a> plugin for <a href="https://code.visualstudio.com/">Visual Studio Code</a> to locally run the website.

## Usage
This webpage contains following sections:
- The Bank
- Work
- Laptop Selection
- Laptop Info

### The Bank
This section displays the user's balance, outstanding loan and gives the option to get a loan.
1. Balance: The displayed balance is the amount available for the user to buy a laptop.
2. Outstanding loan (only visible after taking a Loan): Shows the outstanding Loan. This value can be reduced by transferring money to bank account or by repaying the loan (see section [Work](#work)).
3. Get a Loan button: The Get a loan button will attempt to get a loan from the bank. When the Get a loan button is clicked, it shows a “Prompt” popup box that allows the user to enter an amount.
	* The user cannot get a loan more than double of your bank balance (i.e., if they have 500 €, they cannot get a loan greater than 1,000 €).
	* The user cannot get more than one bank loan before repaying the last loan.
	* The user may not have two loans at once. The initial loan should be paid back in full.

### Work
This section displays the user's current salary and the option to receive money by working. In this section, the user can also pay off debts or transfer money to the bank account.
1. Pay: The user's current salary in €. Should show how much money the user has earned by “working”. This money is NOT part of the user's bank balance.
2. Bank Button: The bank button transfers the money from the user's Pay/Salary balance to their Bank balance.
	* If the user has an outstanding loan, 10% of their salary is deducted and transferred to the outstanding Loan amount.
	* The balance after the 10% deduction is then transferred to the user's bank account.
	* If the outstanding loan is less than 10% of the user's salary, the loan is fully payed back, the remaining amount is transferred to their Bank account.
3. Work button: Increase the user's Pay with 100 € on each click.
4. Repay Loan button: Once the user has a loan, a new button labeled “Repay Loan” appears. Upon clicking this button the full value of their current Pay amount goes towards the outstanding loan and NOT their bank account. Any remaining funds after paying the loan is transferred to their bank account.

### Laptop Selection
The user can select a box to show the available computers. The feature list of the selected laptop is displayed here. Changing a laptop updates the user interface with the information for that selected laptop. The data for the laptops are provided via <a href=https://noroff-komputer-store-api.herokuapp.com/computers>RESTful API that returns JSON data</a>.

### Laptop Info
The Info section is where the image, name, and description as well as the price of the laptop is
displayed.
1. Image: The path to the image of a laptop is received from the response. If no image was found, the image from [here](images/) is displayed. 
2. Buy Now button: The buy now button is the final action of the website. This button will attempt to “Buy” a laptop and
validate whether the bank balance is sufficient to purchase the selected laptop. If the user does not have enough money in the “Bank”, a message must be shown that they cannot afford the laptop. When they have sufficient “Money” in the account, the amount is deducted from the bank and they receive a message that they are now the owner of the new laptop!

## Maintainers

[@SophiaKunze](https://gitlab.com/SophiaKunze).

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-1/-/issues/new).

This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.

### Acknowledgement

This project exists thanks to my teacher <a href="https://gitlab.com/sumodevelopment">Dewald Els</a> and <a href=https://www.experis.de/de>Experis</a>.

## License

[MIT](https://opensource.org/licenses/MIT) © Sophia Kunze